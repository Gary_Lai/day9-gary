##  Daily Summary 

**Time: 2023/7/20**

**Author: 赖世龙**

---

**O (Objective):**  Today, I learned the basic operation of mysql and the use of jpa framework, and also learned the one-to-many relationship of object mapping. Through today's exercises, I have mastered how to use jpa to operate mysql database

**R (Reflective):**  Satisfied

**I (Interpretive):**  Using the jpa framework to operate the database is very convenient, it has helped us to achieve a lot of frequently used methods, is a very powerful framework

**D (Decisional):**  I can use jpa as the ORM framework in springboot project, and it is very convenient to automate database building operations

