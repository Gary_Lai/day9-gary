package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import org.hibernate.criterion.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public EmployeeRepository getEmployeeRepository() {
        return employeeRepository;
    }

    public List<Employee> findAll() {
        return getEmployeeRepository().findAll();
    }

    public Employee findById(Long id) {
        return getEmployeeRepository().findById(id).orElse(null);
    }

    public void update(Long id, Employee employee) {
        Employee toBeUpdatedEmployee = findById(id);
        if (toBeUpdatedEmployee == null) {
            return;
        }
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        employeeRepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return getEmployeeRepository().findByGender(gender);
    }

    public Employee create(Employee employee) {
        return getEmployeeRepository().save(employee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        PageRequest pageSetting = PageRequest.of(page - 1, size);
        return getEmployeeRepository().findAll(pageSetting).getContent();
    }

    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }
}
