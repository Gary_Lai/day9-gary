package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public CompanyRepository getCompanyRepository() {
        return companyRepository;
    }

    public EmployeeRepository getEmployeeRepository() {
        return employeeRepository;
    }

    public List<Company> findAll() {
        return getCompanyRepository().findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return getCompanyRepository().findAll(PageRequest.of(page - 1, size)).getContent();
    }

    public Company findById(Long id) {
        return getCompanyRepository().findById(id).orElseThrow(CompanyNotFoundException::new);
    }

    public void update(Long id, Company company) {
        Optional<Company> optionalCompany = getCompanyRepository().findById(id);
        optionalCompany.ifPresent(previousCompany -> {
            previousCompany.setName(company.getName());
            getCompanyRepository().save(previousCompany);
        });
    }

    public Company create(Company company) {
        return getCompanyRepository().save(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return getEmployeeRepository().findByCompanyId(id);
    }

    public void delete(Long id) {
        companyRepository.deleteById(id);
    }
}
